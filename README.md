# Frameworks JavaScript

## Integrantes

- Navarro Arias Juan Dirceu
- Navarro Arias  Luis Fernando Numa
- Rodriguez Fernandez Bethsy

# Proyecto Final
En base al juego de Sudoku, desarrollar una web que tenga las siguientes características:

- Permita a los usuarios registrar su nombre, ya sea al momento de acceder al juego o al terminar una partida.
- Jugar partidas de Sudoku en pantalla.
- Ver el historial de partidas jugadas en el navegador.
- Empezar una nueva partida cuando el usuario lo desée.
- Resolver el juego en los niveles fácil y medio.
- Tener un tutorial de como jugar Sudoku.

Lo que se requiere: 
1. Implementar SPA
2. Elegir un tipo de SPA, fundamentar la respuesta
3. Elegir un Framework JS con el que trabajar y fundamentar su uso
4. Desarrollar la página web del juego
5. Crear un ficha tecnica del proyecto indicando en resumen las tecnologias y metodologias usadas

El proyecto puede ser desarrollado por grupos de hasta 3 personas.
El producto final debe ser funcional, debe estar probado y utilizar ultimos lineamientos de desarrollo.
Se deberá entregar tanto los recursos webs para levantar el proyecto como  el documento donde indiquen los puntos requeridos anteriomente

## Herramientas

- Vue.js
- Less
- Jquery

#Base 
En el trabajo de Andrey Nering
- https://github.com/andreynering/vuejs-sudoku
y de Jonas Ohlsson
- https://github.com/pocketjoso/sudokuJS

## Levantar el proyecto

Ejecutar el archivo index.html

## Forma de entrega
- Subir el codigo a Github/GitLab.
- Proporcionar el enlace del Github/GitLab en la entrega de proyecto del Moodle con un archivo Word/PDF adjunto donde se explica el proyecto.
